<?php

namespace App\Http\Controllers;

use App\Models\VehicleType;
use App\Models\Vehicle;
use App\Models\VehicleAvailableParameter;
use App\Models\VehicleParameter;
use App\Models\User;
use \Validator;
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;

class VehicleController extends Controller
{
    public function add()
    {
        return view('vehicle.add', [
            'vehicleTypes' => VehicleType::all(),
            'availableParameters' => VehicleAvailableParameter::all(),
        ]);
    }

    public function addAction(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'email|required',
            'password' => 'required|min:6',
            'vehicleType' => 'required|not_in:0',
            'vehicleName' => 'required|min:6|max:255',
            'vehicleDescription' => 'required|min:10',
            'parameters' => new \App\Rules\VehicleParameter($request->get('vehicleType')),
        ]);

        $validator->after(function ($validator) {
            if (!$this->validateOrCreateUser($validator->getData()['email'], $validator->getData()['password'])) {
                $validator->errors()->add('email', 'Email and password not match.');
            }
        });

        if ($validator->fails()) {
            return redirect()
                ->route('add')
                ->withErrors($validator)
                ->withInput();
        }

        $user = User::where('email', '=', $request->get('email'))->first();

        $vehicle = new Vehicle();
        $vehicle->vehicle_type = VehicleType::where('key', $request->get('vehicleType'))->first()->key;
        $vehicle->name = $request->get('vehicleName');
        $vehicle->description = $request->get('vehicleDescription');
        $vehicle->owner_id = $user->id;
        $vehicle->save();

        $vehicleParameters = [];

        foreach ($request->get('parameters') as $parameterKey => $parameterValue){
            if($parameterValue){
                $vehicleParameter = new VehicleParameter();
                $vehicleParameter->vehicle_id = $vehicle->id;
                $vehicleParameter->key = $parameterKey;
                $vehicleParameter->value = $parameterValue;

                $vehicleParameters[] = $vehicleParameter;
            }

        }

        $vehicle->parameters()->saveMany($vehicleParameters);

        return redirect()->back()->withSuccess('Vehicle added.');
    }

    private function validateOrCreateUser($email, $password)
    {
        $user = User::where('email', '=', $email)->first();
        if (!$user) {
            $user = new User();
            $user->name = $email;
            $user->email = $email;
            $user->password = Hash::make($password);
            $user->save();

            return true;
        }
        if (!Hash::check($password, $user->password)) {
            return false;
        }

        return true;
    }
}
