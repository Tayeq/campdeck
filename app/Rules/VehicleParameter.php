<?php

namespace App\Rules;

use App\Models\VehicleType;
use Illuminate\Contracts\Validation\Rule;

class VehicleParameter implements Rule
{
    private $vehicleType;
    private $availableParameters = [];
    private $message = 'Invalid parameters.';
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($vehicleType)
    {
        $this->vehicleType = VehicleType::where('key', $vehicleType)->first();
        if($this->vehicleType){
            $this->availableParameters = $this->vehicleType->availableParameters()->get()->keyBy('key');
        }

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(!$this->vehicleType){
          return false;
        }

        foreach ($value as $parameterName => $parameterValue){
            if(!isset($this->availableParameters[$parameterName])){
                return false;
            }

            if($this->availableParameters[$parameterName]->required && is_null($parameterValue)){
                return false;
            }

            $className = '\App\Rules\VehicleParameters\\'.\Str::ucfirst(\Str::camel($parameterName));

            if(class_exists($className)){
                $validator = \Validator::make([$parameterName => $parameterValue], array(
                    $parameterName => new $className,
                ));

                if($validator->fails()){
                    $this->message = $validator->messages()->first();
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
