<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VehicleType extends Model
{
    use HasFactory;

    public $timestamps = false;

    public function vehicles()
    {
        return $this->hasMany('App\Models\Vehicle', 'vehicle_type', 'key');
    }

    public function availableParameters()
    {
        return $this->hasMany('App\Models\VehicleAvailableParameter', 'vehicle_type', 'key');
    }
}
