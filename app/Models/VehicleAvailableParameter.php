<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VehicleAvailableParameter extends Model
{
    use HasFactory;

    public $timestamps = false;

    public function getCodeAttribute()
    {
        return $this->vehicle_type.'.'.$this->key;
    }
}
