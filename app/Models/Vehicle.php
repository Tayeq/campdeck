<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    use HasFactory;

    public function vehicleType()
    {
        return $this->belongsTo('App\Models\VehicleType', 'vehicle_type', 'key');
    }

    public function availableParameters()
    {
        return $this->vehicleType()->first()->availableParameters();
    }

    public function parameters()
    {
        return $this->hasMany('App\Models\VehicleParameter', 'vehicle_id', 'id');
    }

    public function owner(){
        return $this->belongsTo('App\Models\User', 'owner_id', 'id');
    }
}
