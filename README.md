## Database Project
https://dbdiagram.io/d/5f8196d23a78976d7b771acb

## How to install

1. Run composer
    ```
    composer install
    ```
2. Run migrations
    ```
    php artisan migrate
    ```
3. (optional) Run DB seed
    ```
    php artisan db:seed
    ```
