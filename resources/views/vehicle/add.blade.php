@extends('layouts.app')

@section('content')
    <div class="container pt-5">
        {{ Form::open(['url' => route('addAction'), 'method' => 'post']) }}
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(session('success'))
            <div class="alert alert-success">
                {{session('success') }}
            </div>
        @endif

        <h3>Logowanie/Rejestracja</h3>
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
        </div>

        <div class="form-group ">
            <label for="password">Password:</label>
            <input type="password" class="form-control" id="password" name="password">
        </div>

        <hr/>

        <h3>Dodaj pojazd</h3>
        <div class="form-group">
            <label for="vehicleType">Typ Pojazdu:</label>
            <select class="form-control " id="vehicleType" name="vehicleType">
                <option value="0">-- Wybierz typ pojazdu --</option>
                @foreach($vehicleTypes as $vehicleType)
                    <option value="{{$vehicleType->key}}" @if($vehicleType->key == old('vehicleType')) selected @endif>{{$vehicleType->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="vehicleName">Nazwa pojazdu:</label>
            <input type="text" placeholder="np. Volvo 15" class="form-control" id="vehicleName" name="vehicleName" value="{{ old('vehicleName') }}">
        </div>

        <div class="form-group">
            <label for="vehicleDescription">Opis pojazdu:</label>
            <textarea placeholder="Szczegółowy opis pojazdu..." class="form-control" id="vehicleDescription"
                      name="vehicleDescription">{{ old('vehicleDescription') }}</textarea>
        </div>

        <hr/>

        <h4>Parametry pojazdu</h4>
        @foreach($availableParameters as $availableParameter)
            <div class="form-group parameter" data-vehicle-type="{{$availableParameter->vehicle_type}}"
                 style="display: none">
                <label for="{{$availableParameter->code}}">{{$availableParameter->name}}
                    : @if($availableParameter->required) <span class="text-danger">*</span> @endif</label>
                <input type="text" class="form-control" id="{{$availableParameter->code}}"
                       name="parameters[{{$availableParameter->key}}]" value="{{old('parameters.'.$availableParameter->key)}}"
                       @if($availableParameter->required) required @endif >
            </div>
        @endforeach

        <div class="form-group text-center">
            <button type="submit" class="btn btn-primary w-25">Dodaj</button>
        </div>

        {{ Form::close() }}

    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        {{--    Najszybsze i najłatwiejsze rozwiązanie mogę zmienić na coś innego ale szkoda czasu na tworzenie webpacków dla przykładowej aplikacji ;)  --}}
        $('form #vehicleType').on('change', function () {
            var selectedType = $(this).val();

            $('form .parameter').hide();
            $('form .parameter').find('input').attr('disabled', true);

            $('form .parameter[data-vehicle-type="' + selectedType + '"]').show();
            $('form .parameter[data-vehicle-type="' + selectedType + '"]').find('input').attr('disabled', false);
        });

        $('form #vehicleType').trigger('change');
    </script>
@endsection
