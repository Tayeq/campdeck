<?php

namespace Database\Seeders;

use App\Models\VehicleAvailableParameter;
use App\Models\VehicleType;
use Illuminate\Database\Seeder;

class VehicleDefaultSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            ['key' => 'boat', 'name' => 'Łódź'],
            ['key' => 'camper', 'name' => 'Kamper'],
        ];

        foreach ($types as $type){
            $vehicleType = VehicleType::firstOrCreate($type);

            $availableParameters = [
                ['key' => 'price', 'name' => 'Cena', 'required' => true, 'vehicle_type' => $vehicleType->key],
                ['key' => 'toilet', 'name' => 'Toaleta', 'required' => false, 'vehicle_type' => $vehicleType->key],
                ['key' => 'seats', 'name' => 'Liczba miejsc', 'required' => false, 'vehicle_type' => $vehicleType->key],
            ];

            foreach ($availableParameters as $availableParameter){
                VehicleAvailableParameter::firstOrCreate($availableParameter);
            }
        }
    }
}
