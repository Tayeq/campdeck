<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_types', function (Blueprint $table) {
            $table->string('key')->index();
            $table->string('name');
            $table->unique('key');
        });

        Schema::create('vehicles', function (Blueprint $table) {
            $table->id();
            $table->string('vehicle_type');
            $table->string('name');
            $table->longText('description');
            $table->bigInteger('owner_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('vehicle_type')->references('key')->on('vehicle_types');
            $table->foreign('owner_id')->references('id')->on('users');
        });

        Schema::create('vehicle_parameters', function (Blueprint $table) {
            $table->bigInteger('vehicle_id')->unsigned();
            $table->string('key')->index();
            $table->string('value');
            $table->unique(['vehicle_id', 'key']);

            $table->foreign('vehicle_id')->references('id')->on('vehicles');
        });

        Schema::create('vehicle_available_parameters', function (Blueprint $table) {
            $table->string('key')->index();
            $table->string('vehicle_type');
            $table->string('name');
            $table->boolean('required');
            $table->unique(['vehicle_type', 'key']);

            $table->foreign('vehicle_type')->references('key')->on('vehicle_types');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('vehicle_types');
        Schema::dropIfExists('vehicles');
        Schema::dropIfExists('vehicle_parameters');
        Schema::dropIfExists('vehicle_available_parameters');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
